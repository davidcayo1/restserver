/* 
  Variables sensibles.- el puerto, variables que tengan credenciales en el backend
*/
// Obtenemos el paquete dotenv
require('dotenv').config();
const Server = require('./models/server');

const server = new Server();

server.listen();