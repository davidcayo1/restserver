const { request, response } = require("express");


const usuarioGet = (req = request, res = response) => {
  // const query = req.query;
  const { nombre, key = 'no key' } = req.query;

  res.json({
    msg: 'get API - controlador',
    nombre,
    key
  });

}

const usuarioPut = (req, res = response) => {
  /* res.json({
    msg: 'get API - Put'
  }); */

  // const id = req.params.id;
  const { id } = req.params;

  res.json({
    msg: 'put API - controller',
    id
  })
}

const usuarioPost = (req, res = response) => {
  /* res.json({
    msg: 'get API - Post'
  }); */

  //req.body trae la informacion del body
  const { nombre, edad } = req.body;

    res.status(201).json({
      msg: 'post API',
      nombre,
      edad
    });
  
}

const usuarioDelete = (req, res = response) => {
  res.json({
    msg: 'delete API - controlador'
  });

}


module.exports = {
  usuarioGet,
  usuarioPut,
  usuarioPost,
  usuarioDelete
}