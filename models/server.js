const express = require('express');

class Server {

  constructor() {
    this.app = express();
    this.port = process.env.PORT;
    this.usuariosPath = '/api/usuarios';

    // Middlewares, es lo primero que se carga en una aplicación
    this.middlewares();
    // Rutas de mi aplicación
    this.routes();
  }

  middlewares() {
    //Lectura y parseo del body
    //Cualquier informacion que venga se serializara en formato JSON
    this.app.use(express.json());
    
    // Se publica la carpeta public
    this.app.use(express.static('public'));
  }

  routes() {
    // Este es un middleware donde this.usuarioPath que es igual a '/api/usuarios/' hara referencia a cada ruta y con require estamos queririendo las rutas de user.js para usarlo cada uno
    this.app.use(this.usuariosPath, require('../routes/user'))
  }

  listen() {
    // Con process.env.PORT obtenemos el valor de la variable PORT que esta en el archivo .env
    this.app.listen(this.port, () => {
      console.log('Servidor corriendo en puerto ', this.port);
    })
  }

}

module.exports = Server;
