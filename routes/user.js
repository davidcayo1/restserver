const { Router } = require('express');
const { usuarioGet, usuarioPut, usuarioPost, usuarioDelete } = require('../controllers/user');

const router = Router();

 /*  router.get('/', (req, res) => {
    res.json({
    msg: 'get API'
    });
  }); */

  router.get('/', usuarioGet);
  router.put('/:id', usuarioPut);
  router.post('/', usuarioPost);
  router.delete('/', usuarioDelete);

  
module.exports = router;